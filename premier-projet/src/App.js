import React, { Component, Fragment } from 'react';
import './App.css';
import Membre from './components/Membre'
import button from './components/Button'
import Button from './components/Button';

const famille = 
{
  membre1: {
    nom: 'Thibaut',
    age: 21 
  },
  membre2: {
    nom: 'Marine',
    age: 23 
  },
  membre3: {
    nom: 'Sandrine',
    age: 45 
  }
}

class App extends Component {
state = {
  famille
}

handleClick = (num) => {
  const famille = {... this.state.famille}
  famille.membre1.age += num
  this.setState({ famille })
}
handleChangeAge = event => {
  const famille = {... this.state.famille}
  const age = event.target.value
  console.log =(age)
  famille.membre1.age = age
  this.setState({ famille })
}
handleChangeNom = event => {
  const famille = {... this.state.famille}
  const nom = event.target.value
  console.log =(nom)
  famille.membre1.nom = nom
  this.setState({ famille })
}

  render() {
    const { title } = this.props
    const { famille } = this.state

    return (

      <div className="App">
        <h1>{title}</h1>
      <input value={famille.membre1.nom} onChange={this.handleChangeNom} type="text"/>
        <Membre
          age={famille.membre1.age}
          nom={famille.membre1.nom}/>
        <Membre
          age={famille.membre2.age}       
          nom={famille.membre2.nom}/>
        <Membre
          age={famille.membre3.age}  
          nom={famille.membre3.nom}>
          <strong>Je suis un chien</strong>
          
        </Membre>
        <Button vieillir={() => this.handleClick(2)}/>
      </div>  

    );
  }
}

export default App;
