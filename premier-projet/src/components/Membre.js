import React, {Fragment} from 'react' 
import App from '../App'

const Membre = ({nom, age, children}) => {
  
  return (
    <Fragment>
      <h2 style={{ 
        backgroundColor: age < 30 ? '#6fa' : '#4d5', 
        color: '#fff' }}
      >          
        {nom.toUpperCase()} : {age}
      </h2>
      {children ? <p>{children}</p> : <Fragment/>}
    </Fragment>
    
  )
}


export default Membre